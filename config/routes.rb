Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  root 'pages#index'
  get 'editor', to: 'pages#editor'
  get 'admin', to: 'pages#admin'

  resources :appointments
  post 'appointments_ajax', to: 'appointments#ajax'

  resources :sessions, only: [:destroy, :new, :create]


end
