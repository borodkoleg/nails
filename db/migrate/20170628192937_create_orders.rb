class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.belongs_to :client
      t.datetime :time
      t.timestamps
    end
  end
end
