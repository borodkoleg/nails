class Adddate < ActiveRecord::Migration
  def change
    add_column :orders, :date, :date
    change_column :orders, :time, :time
  end
end
