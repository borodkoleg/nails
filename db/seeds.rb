# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Setting.delete_all
# Setting.create(name:'gmail_username', typ:'string', value: Rails.application.secrets.gmail_username)
# Setting.create(name:'gmail_password', typ:'password', value: Rails.application.secrets.gmail_password)
Setting.create(name:'admin_email', typ:'email', value: Rails.application.secrets.admin_email)
Setting.create(name:'site_domain', typ:'string', value: Rails.application.secrets.site_domain)
Setting.create(name:'site_name', typ:'string', value: Rails.application.secrets.site_name)
Setting.create(name:'admin_password', typ:'password', value: Rails.application.secrets.admin_password)
Setting.create(name:'admin_name', typ:'password', value: Rails.application.secrets.admin_name)
Setting.create(name:'weekend_days_week', typ:'tags', value: 'sunday')
Setting.create(name:'time_start_work', typ:'time_picker', value: '08')
Setting.create(name:'time_end_work', typ:'time_picker', value: '20')
Setting.create(name:'time_step', typ:'number', value: '30')
Setting.create(name:'weekend_days', typ:'string', value: nil)
