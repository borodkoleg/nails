ActiveAdmin.register Setting do
  #actions :index, :show, :new, :create, :update, :edit, :destroy
  actions :index, :update, :edit
  permit_params :name, :value, :typ
  before_filter :skip_sidebar!

  index do
    column :name
    column :value, sortable: :value do |el|
      el == 'weekend_days'
      if el.name == 'admin_name'
        '*'
      elsif el.name == 'admin_password'
        '*'
      else
        el.value
      end
    end
    column :actions do |object|
      # raw( %(#{link_to "View", [:admin, object]}
      #      #{link_to "Delete", [:admin, object], method: :delete}
      #      #{(link_to"Edit", [:edit, :admin, object]) }))
      if object.name != 'weekend_days'
        raw( %(#{(link_to "Edit", [:edit, :admin, object]) }) )
      else
        raw( %(#{(link_to "Edit", admin_weekend_days_path) }) )
      end

    end
  end

  form do |f|
    f.inputs "Upload" do
      f.input :name
      if f.object.typ == 'tags'
        f.input :value, as: :tags, collection: ['sunday', 'monday', 'tuesday','wednesday','thursday','friday','saturday']
      elsif f.object.typ == 'time_select'
        # f.input :value, as: :time_select
        f.input :value, as: :time_picker
      else
        f.input :value, as: f.object.typ
      end
    end
    f.actions
  end
end