ActiveAdmin.register Order do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  permit_params :date, :time, :image, :client_id

  index do
    selectable_column
    id_column
    column :client
    column :date
    column :time, sortable: :time do |firmware| firmware.time.strftime("%I:%M %p") end
    column 'Image', sortable: :image_file_name do |firmware| link_to firmware.image_file_name, firmware.image.url end
    actions
  end

  form do |f|
    f.inputs "Upload" do
      f.input :client
      f.input :date, as: :datepicker, datepicker_options: { dateFormat: "mm/dd/yy" }
      f.input :time, as: :time_select

      f.input :image, required: true, as: :file
    end
    f.actions
  end

end
