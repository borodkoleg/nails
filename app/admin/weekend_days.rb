ActiveAdmin.register_page "Weekend days" do
  page_action :ajax, method: :post do
    data = params[:dates]
    data = data.to_s
    # mas = data.split(",")
    set = Setting.find_by name: 'weekend_days'
    if set
      set.value = data
      set.save
      render js: 'success', status: 200
    else
      render js: 'error'
    end
  end

  controller do
    def index
      @setting_weekend_days = Setting.weekend_days
      render 'index', :layout => 'active_admin'
    end
  end

  # content do
  #   weekend_days = Setting.find_name('weekend_days')
  #   if weekend_days
  #     unless weekend_days.strip.empty?
  #       @setting_weekend_days = weekend_days.split(",")
  #       render 'index'
  #     else
  #       @setting_weekend_days = []
  #       render 'index'
  #     end
  #   else
  #     @setting_weekend_days = []
  #     render 'index'
  #   end
  #
  # end

end