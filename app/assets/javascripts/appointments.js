$(document).ready(function() {
  $(function () {
    $('#datetimepicker_day').datetimepicker({
      inline: true,
      sideBySide: true,
      format: 'YYYY-MM-DD',
      daysOfWeekDisabled: Setting_mas_weekend,
      disabledDates: Setting_weekend_days
    });
  });

  $(function () {
    $('#datetimepicker_time').datetimepicker({
      inline: true,
      sideBySide: true,
      format: 'LT',
      stepping: Setting_time_step,
      enabledHours: Setting_time_work
    });
  });

  $("#datetimepicker_day").on("dp.change", function(e) {
    //alert($('#datetimepicker12').data('date'));

    $.ajax({
      type: "POST",
      url: "appointments_ajax",
      data: "date=" + $('#datetimepicker_day').data('date'),
      //dataType: "JSON" // you want a difference between normal and ajax-calls, and json is standard
      success:function(data){
        result = "";

        for (i = 0; i <= 23; i++) {
          if (data[i]) {
            result = result + "<span class='get_time'>" + data[i] + "</span>";
          }
        }
        // $("#time").html(JSON.stringify(data));
        $("#time").html(result);
      },
      error:function(data){

      }
    });
  });

  $('#appointments_file').on('change', function(event, numFiles, label) {
    //=================validation image
    function getExtension(filename) {
      var parts = filename.split('.');
      return parts[parts.length - 1];
    }

    function isImage(filename) {
      var ext = getExtension(filename);
      switch (ext.toLowerCase()) {
        case 'jpg':
        case 'gif':
        case 'bmp':
        case 'png':
          //etc
          return true;
      }
      return false;
    }

    var image = $('#appointments_file').val();
    if (!isImage(image)){
      $('#appointments_file').val("");
      $('.valid-file').addClass("fa-times").removeClass("fa-check");
    }else{
      $('.valid-file').addClass("fa-check").removeClass("fa-times");
    }
    //=================

  });

  $(".new_order").submit(function () {

    // var valuesToSubmit = $(this).serialize();
    var date = $('#datetimepicker_day').data('date');
    var time = $('#datetimepicker_time').data('date');
    // $('#appointments_name').val();
    // $('#appointments_email').val();
    // alert($('#appointments_file').val());
    // alert(valuesToSubmit);

    //var myForm = $(".new_order");
    var formData = new FormData(this);
    formData.append('appointments[date]', date);
    formData.append('appointments[time]', time);

    $.ajax({
      type: "POST",
      url: "appointments",
      data: formData,
       processData: false,
       contentType: false,
      // cache: false
      dataType: 'JSONP',
      error: function(response) {
      console.log(response.status + " " + response.statusText);
    }
    });

    return false;
  });

});