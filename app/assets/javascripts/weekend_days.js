$(document).ready(function() {
  $('#mdp-demo').multiDatesPicker({
    addDates: Setting_weekend_days,
    dateFormat: "yy-mm-dd",
    onSelect: function(selected,evnt){
      result = $(this).multiDatesPicker('getDates').toString().replace (/,/g, '<br>');
      $("#mdp-result").html(result);
    }
  });

  $("#weekend_days_save").click(function(){
    var dates = $('#mdp-demo').multiDatesPicker('getDates').toString();

    $.ajax({
      type: "POST",
      url: "weekend_days/ajax",
      data: "dates=" + dates,
      success: function(response){

        window.location = "/admin/weekend_days";
      },
      error: function(response) {

        if (response.status == '200'){
          window.location = "/admin/weekend_days";
        }else{

        }

      }

    });
  });

  result = $('#mdp-demo').multiDatesPicker('getDates').toString().replace (/,/g, '<br>');
  $("#mdp-result").html(result);
});