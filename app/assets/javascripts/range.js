$(document).ready(function() {

//==========range
  var slider_skintone = document.getElementById('skintone_range');
  noUiSlider.create(slider_skintone, {
    start: [5],
    step: 1,
    range: {
      min: [0],
      max: [10]
    },
    pips: {
      mode: 'values',
      values: [],
      density: 10
    }
  });

  slider_skintone.noUiSlider.on('hover', function (value) {
    //console.log(value);
  });

  slider_skintone.addEventListener('keydown', function( e ) {

    var value = Number( slider_skintone.noUiSlider.get() );

    switch ( e.which ) {
      case 37: slider_skintone.noUiSlider.set( value - 1 );
        break;
      case 39: slider_skintone.noUiSlider.set( value + 1 );
        break;
    }
  });

  slider_skintone.noUiSlider.on('change', function( values, handle ){
    // alert(values);
  });

  //=========================================

  var slider_nail_length = document.getElementById('nail_length_range');
  noUiSlider.create(slider_nail_length, {
    start: [5],
    step: 1,
    range: {
      min: [0],
      max: [10]
    },
    pips: {
      mode: 'values',
      values: [],
      density: 10
    }
  });

  slider_nail_length.addEventListener('keydown', function( e ) {

    var value = Number( slider_nail_length.noUiSlider.get() );

    switch ( e.which ) {
      case 37: slider_nail_length.noUiSlider.set( value - 1 );
        break;
      case 39: slider_nail_length.noUiSlider.set( value + 1 );
        break;
    }
  });

  slider_nail_length.noUiSlider.on('change', function( values, handle ){

  });


//===========



});