module SessionsHelper
  def solt
    "sdfweweevcrtgthw1qw3Qweecvewvcw432WEewre324teFveghre"
  end

  def user_admin?
    session[:user_admi] == token_encode
  end

  private

  def token_encode
    payload = { key: solt }
    token = JWT.encode payload, solt, 'HS256'

    Base64.strict_encode64(token)
  end

  # tok = Base64.strict_decode64(token)
  # decoded_token = JWT.decode tok, solt, true, { algorithm: 'HS256' }
end
