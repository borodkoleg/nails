class AppointmentsController < ApplicationController
  skip_before_action :verify_authenticity_token #for ajax jquery

  def index
    @setting_weekend_days = Setting.weekend_days

    day_off = Setting.find_name('weekend_days_week')
    if day_off
      mas = day_off.split(",")
      @mas_weekend = []
      mas.each do |x|
        @mas_weekend << 0 if x == 'sunday'
        @mas_weekend << 1 if x == 'monday'
        @mas_weekend << 2 if x == 'tuesday'
        @mas_weekend << 3 if x == 'wednesday'
        @mas_weekend << 4 if x == 'thursday'
        @mas_weekend << 5 if x == 'friday'
        @mas_weekend << 6 if x == 'saturday'
      end
    else
      @mas_weekend = [0, 6]
    end

    time_start_work = Setting.find_name('time_start_work')
    time_end_work = Setting.find_name('time_end_work')
    if time_start_work && time_end_work
      @time_start_work = time_start_work.to_i
      @time_end_work = time_end_work.to_i
    else
      @time_start_work = 8
      @time_end_work = 20
    end

    time_step = Setting.find_name('time_step')
    if time_step
      if is_number?(time_step)
        @setting_time_step = time_step
        return
      end
    end
    @setting_time_step = 30
  end

  def ajax
    date = params[:date].to_date
    orders = Order.where(date: date).as_json

    result = {}
    orders.each_with_index do |el, index|
      result[index] = el["time"].strftime("%I:%M %p")
    end

    render json: result
  end

  def create
    #check time and date
    test = Order.new(time: person_params[:time])
    order_exist = Order.find_by(date: person_params[:date], time: test.time)
    if order_exist
      flash[:error] = { error: ["This time is busy"] }
      redirect_to action: "index"
      return
    end

    client = Client.find_by email: person_params[:email]
    unless client
      client_new = Client.new(name: person_params[:name], email: person_params[:email])
      if client_new.save
        client = client_new
      else
        flash[:error] = client_new.errors
        redirect_to action: "index"
        return
      end
    end

    order = Order.new(time: person_params[:time], date: person_params[:date], image: person_params[:image])
    order.client = client
    if order.save
      flash[:message] = "Order added"
      EmailSendJob.perform_later(client, order)
      redirect_to action: "index"
    else
      flash[:error] = order.errors
      redirect_to action: "index"
      return
    end

  end

  private

  def person_params
    params.require(:appointments).permit(:email, :name, :date, :time, :image)
  end

end
