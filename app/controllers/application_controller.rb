class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include SessionsHelper

  private

  def is_number?(el)
    el.to_f.to_s == el.to_s || el.to_i.to_s == el.to_s
  end

  def authenticate_admin_user!
    redirect_to new_session_path unless user_admin?
  end
end
