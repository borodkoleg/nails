class SessionsController < ApplicationController
  def new
    render layout: 'application_nil'
  end

  def create
    admin_password = Setting.find_name('admin_password')
    admin_name = Setting.find_name('admin_name')
    if admin_password && admin_name
      admin_password = Setting.decrypt(admin_password)
      admin_name = Setting.decrypt(admin_name)
    else
      redirect_to admin_root_path
      return
    end

    if person_params[:password] == admin_password &&
           person_params[:name] == admin_name
      session[:user_admi] = token_encode
      redirect_to admin_root_path
    else
      redirect_to action: :new
    end
  end

  def destroy
    session.delete(:user_admi)
    reset_session
    redirect_to root_url
  end

  private

  def person_params
    params.require(:sessions).permit(:name, :password)
  end

end
