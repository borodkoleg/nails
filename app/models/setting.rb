class Setting < ActiveRecord::Base
  def self.crypt
    ActiveSupport::MessageEncryptor.new('wsdfwer wedf wed wedwe d324rt34t54yg4 grefgerfewf34r3 4r3fewrferf34 34rfewrf')
  end

  before_create do
    if self.typ == 'password'
      key = Setting.crypt
      self.value = key.encrypt_and_sign(self.value)
    end
  end

  def self.decrypt(data)
    key = Setting.crypt
    key.decrypt_and_verify(data)
  end

  def self.find_name(data)
    result = Setting.find_by name: data
    if result
      result.value
    else
      nil
    end
  end

  def self.weekend_days
    weekend_days = Setting.find_name('weekend_days')
    if weekend_days
      unless weekend_days.strip.empty?
        return weekend_days
      end
    end
    return false
  end

end