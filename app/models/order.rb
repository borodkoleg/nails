class Order < ActiveRecord::Base
  belongs_to :client

  has_attached_file :image,
                    :url => "/images/:client_id/:style/:basename.:extension",
                    :path => ":rails_root/public/images/:client_id/:style/:basename.:extension",
                    :default_url => "/images/:client_id/:style/example_data.csv", # :id - (id orders)
                    styles: { thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates_attachment_size :image, less_than: 1000000

  Paperclip.interpolates :client_id do |attachment, style|
    attachment.instance.client.id
  end

  validates :time, presence: true
  validates :date, presence: true
end
