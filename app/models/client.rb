class Client < ActiveRecord::Base
  has_many :orders

  validates :name, presence: true, length: { in: 2..100 }
  validates_email_format_of :email
  validates :email, presence: true, uniqueness: { case_sensitive: false }

  before_save do
    self.email = email.downcase
  end
end
