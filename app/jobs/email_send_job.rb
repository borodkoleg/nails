class EmailSendJob < ActiveJob::Base
  queue_as :default

  def perform(*params)
    OrderMailer.from_user(*params).deliver_now
    OrderMailer.from_admin(*params).deliver_now
  end
end
