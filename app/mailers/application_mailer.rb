class ApplicationMailer < ActionMailer::Base
  default from: Rails.application.secrets.gmail_username
  layout 'mailer'
end
