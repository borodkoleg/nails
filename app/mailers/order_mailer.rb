class OrderMailer < ApplicationMailer

  def from_user(*params)
    mail to: params[0].email, subject: "Thank You"
    # mail to: Rails.application.secrets.admin_email, subject: "New order"
  end

  def from_admin(*params)
    @order = {}
    @order[:email] = params[0].email
    @order[:name] = params[0].name
    @order[:date] = params[1].date
    @order[:time] = params[1].time
    @order[:image] = params[1].image
    mail to: Setting.find_name('admin_email'), subject: "New order"
  end

end
